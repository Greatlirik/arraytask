package Intexsoft.Training.Zhuk.Tools;

import Intexsoft.Training.Zhuk.Tasks.Task1.Task1;
import Intexsoft.Training.Zhuk.Tasks.Task2.Task2;
import Intexsoft.Training.Zhuk.Tasks.Task3.Task3;
import Intexsoft.Training.Zhuk.Tasks.Task4.Task4;
import Intexsoft.Training.Zhuk.Tasks.Task5.Task5;
import Intexsoft.Training.Zhuk.Tasks.Task6.Task6;
import Intexsoft.Training.Zhuk.Tasks.Task7.Task7;
import Intexsoft.Training.Zhuk.Tasks.Task8.Task8;

import java.util.ArrayList;

public class CreationListOfTasks {

    public static ArrayList<ExecuteTask> getListOfTasks() {

        ArrayList<ExecuteTask> executeTasks = new ArrayList<>();
        executeTasks.add(0,new Task1());
        executeTasks.add(1,new Task2());
        executeTasks.add(2,new Task3());
        executeTasks.add(3,new Task4());
        executeTasks.add(4,new Task5());
        executeTasks.add(5,new Task6());
        executeTasks.add(6,new Task7());
        executeTasks.add(7,new Task8());

        return executeTasks;
    }


}
