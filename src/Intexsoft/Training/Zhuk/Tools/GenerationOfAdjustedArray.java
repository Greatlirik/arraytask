package Intexsoft.Training.Zhuk.Tools;

import java.util.Random;
import java.util.Scanner;

public class GenerationOfAdjustedArray {


    public static String inputValue() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }



    public static int setSize() {
        System.out.println("Введите размерность массива size");
        return TreatmentScannerValue.checkPositive(inputValue());
    }


    public static int setStartRange() {
        System.out.println("Введите начало диапазона n");
        return TreatmentScannerValue.transformInputToInt(inputValue());
    }


    public static int setEndRange() {
        System.out.println("Введите конец диапазона m");
        return TreatmentScannerValue.transformInputToInt(inputValue());
    }

    public static void fillRandomArray(int[]createdArray, int n, int m){
        Random random = new Random();
        for (int index = 0; index < createdArray.length; index++) {
            createdArray[index] = n + random.nextInt(m - n);
        }
    }

    public static int[] generateArrayFromInputValues() {

        int size = setSize();
        int n = setStartRange();
        int m = setEndRange();

        while (!TreatmentScannerValue.checkIsMinLessMax(n, m)) {
            System.out.println("Неверно введено значение, введите начало диапазона n");
            n = setStartRange();
            System.out.println("Введите конец диапазона m");
            m = setEndRange();
        }

        int[] createdArray = new int[size];

        fillRandomArray(createdArray,n,m);

        return createdArray;
    }
}


