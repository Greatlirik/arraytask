package Intexsoft.Training.Zhuk.Tools;


public class ChoiceOfTask {

    private static int inputNumberOfTask(){

        System.out.println("Введите номер задачи");

        int numberOfTask = TreatmentScannerValue.transformInputToInt(GenerationOfAdjustedArray.inputValue());

        return numberOfTask;
    }



    public static ExecuteTask chooseNumberOfTask() {

        int numberOfTask = inputNumberOfTask();

        return switch (numberOfTask) {
            default -> CreationListOfTasks.getListOfTasks().get(0);
            case 2 -> CreationListOfTasks.getListOfTasks().get(1);
            case 3 -> CreationListOfTasks.getListOfTasks().get(2);
            case 4 -> CreationListOfTasks.getListOfTasks().get(3);
            case 5 -> CreationListOfTasks.getListOfTasks().get(4);
            case 6 -> CreationListOfTasks.getListOfTasks().get(5);
            case 7 -> CreationListOfTasks.getListOfTasks().get(6);
            case 8 -> CreationListOfTasks.getListOfTasks().get(7);
        };
    }
}
