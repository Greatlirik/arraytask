package Intexsoft.Training.Zhuk.Tools;


public class TreatmentScannerValue {

    private static boolean checkInputIsDigit(String inputValue) throws NumberFormatException {
        try {
            Integer.parseInt(inputValue);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    public static int transformInputToInt(String inputValue) {
        while (!checkInputIsDigit(inputValue)) {
            System.out.println("Неверно введено значение, введите корректное значение");
            inputValue = GenerationOfAdjustedArray.inputValue();
            checkInputIsDigit(inputValue);
        }
        return Integer.parseInt(inputValue);
    }


    public static int checkPositive(String inputValue) {
        while (transformInputToInt(inputValue) <= 0) {
            System.out.println("Данное число может быть только положительным");
            inputValue = GenerationOfAdjustedArray.inputValue();
            checkInputIsDigit(inputValue);
            checkPositive(inputValue);
        }
        return Integer.parseInt(inputValue);
    }


    public static boolean checkIsMinLessMax(int n, int m) {

        return m > n;
    }



}



